ALTER TABLE `sma_suspended_bills`
 ADD `cgst` DECIMAL(25,4) NULL,
 ADD `sgst` DECIMAL(25,4) NULL,
 ADD `igst` DECIMAL(25,4) NULL;
ALTER TABLE `sma_suspended_items`
 ADD `gst` VARCHAR(20) NULL,
 ADD `cgst` DECIMAL(25,4) NULL,
 ADD `sgst` DECIMAL(25,4) NULL,
 ADD `igst` DECIMAL(25,4) NULL;

UPDATE `sma_pos_settings` SET `version` = '3.2.5' WHERE `pos_id` = 1;
