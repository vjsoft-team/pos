<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('calculateGST')) {
    function calculateGST($cgst, $sgst, $igst, $qty) {
        // if (empty($cgst) && empty($sgst) && empty($igst)) {
        //     return ['gst' => 0];
        // }
        $gst = 0;
        $gst += !empty($cgst) ? ($cgst['amount'] * $qty) : 0;
        $gst += !empty($sgst) ? ($sgst['amount'] * $qty) : 0;
        $gst += !empty($igst) ? ($igst['amount'] * $qty) : 0;
        $gst_data = [
            'gst' => $gst,
            'cgst_id' => $sgst ? $sgst['id'] : NULL,
            'cgst_tax' => $sgst ? $cgst['tax'] : NULL,
            'cgst_val' => $sgst ? $cgst['amount']: NULL,
            'sgst_id' => $sgst ? $sgst['id'] : NULL,
            'sgst_tax' => $sgst ? $sgst['tax'] : NULL,
            'sgst_val' => $sgst ? $sgst['amount'] : NULL,
            'igst_id' => $igst ? $igst['id'] : NULL,
            'igst_tax' => $igst ? $igst['tax'] : NULL,
            'igst_val' => $igst ? $igst['amount'] : NULL,
        ];
        return $gst_data;
    }
}

if ( ! function_exists('getIndianStates')) {
    function getIndianStates($blank = false) {
        $istates  = [
            '35' => 'Andaman & Nicobar',
            '28' => 'Andhra Pradesh',
            '12' => 'Arunachal Pradesh',
            '18' => 'Assam',
            '10' => 'Bihar',
            '04' => 'Chandigarh',
            '22' => 'Chhattisgarh',
            '26' => 'Dadra and Nagar Haveli',
            '25' => 'Daman & Diu',
            '07' => 'Delhi',
            '30' => 'Goa',
            '24' => 'Gujarat',
            '06' => 'Haryana',
            '02' => 'Himachal Pradesh',
            '01' => 'Jammu & Kashmir',
            '20' => 'Jharkhand',
            '29' => 'Karnataka',
            '32' => 'Kerala',
            '31' => 'Lakshadweep',
            '23' => 'Madhya Pradesh',
            '27' => 'Maharashtra',
            '14' => 'Manipur',
            '17' => 'Meghalaya',
            '15' => 'Mizoram',
            '13' => 'Nagaland',
            '21' => 'Odisha',
            '34' => 'Puducherry',
            '03' => 'Punjab',
            '08' => 'Rajasthan',
            '11' => 'Sikkim',
            '33' => 'Tamil Nadu',
            '36' => 'Telangana',
            '16' => 'Tripura',
            '09' => 'Uttarakhand',
            '05' => 'Uttar Pradesh',
            '19' => 'West Bengal',
        ];
        if ($blank) {
            array_unshift($istates, lang('select'));
        }
        return $istates;
    }
}
