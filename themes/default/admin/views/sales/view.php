<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '.sledit', function (e) {
            if (localStorage.getItem('slitems')) {
                e.preventDefault();
                var href = $(this).attr('href');
                bootbox.confirm("<?=lang('you_will_loss_sale_data')?>", function (result) {
                    if (result) {
                        window.location.href = href;
                    }
                });
            }
        });
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-file"></i><?= lang("sale_no") . ' ' . $inv->id; ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>">
                        </i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <?php if ($inv->attachment) { ?>
                            <li>
                                <a href="<?= admin_url('welcome/download/' . $inv->attachment) ?>">
                                    <i class="fa fa-chain"></i> <?= lang('attachment') ?>
                                </a>
                            </li>
                        <?php } ?>
                        <li>
                            <a href="<?= admin_url('sales/edit/' . $inv->id) ?>" class="sledit">
                                <i class="fa fa-edit"></i> <?= lang('edit_sale') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= admin_url('sales/payments/' . $inv->id) ?>" data-target="#myModal" data-toggle="modal">
                                <i class="fa fa-money"></i> <?= lang('view_payments') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= admin_url('sales/add_payment/' . $inv->id) ?>" data-target="#myModal" data-toggle="modal">
                                <i class="fa fa-dollar"></i> <?= lang('add_payment') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= admin_url('sales/email/' . $inv->id) ?>" data-target="#myModal" data-toggle="modal">
                                <i class="fa fa-envelope-o"></i> <?= lang('send_email') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= admin_url('sales/pdf/' . $inv->id) ?>">
                                <i class="fa fa-file-pdf-o"></i> <?= lang('export_to_pdf') ?>
                            </a>
                        </li>
                        <?php if ( ! $inv->sale_id) { ?>
                        <li>
                            <a href="<?= admin_url('sales/add_delivery/' . $inv->id) ?>" data-target="#myModal" data-toggle="modal">
                                <i class="fa fa-truck"></i> <?= lang('add_delivery') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= admin_url('sales/return_sale/' . $inv->id) ?>">
                                <i class="fa fa-angle-double-left"></i> <?= lang('return_sale') ?>
                            </a>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <?php if (!empty($inv->return_sale_ref) && $inv->return_id) {
                    echo '<div class="alert alert-info no-print"><p>'.lang("sale_is_returned").': '.$inv->return_sale_ref;
                    echo ' <a data-target="#myModal2" data-toggle="modal" href="'.admin_url('sales/modal_view/'.$inv->return_id).'"><i class="fa fa-external-link no-print"></i></a><br>';
                    echo '</p></div>';
                } ?>
                <!-- <div class="print-only col-xs-12">
                    <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>" alt="<?= $biller->company != '-' ? $biller->company : $biller->name; ?>">
                </div> -->
                <div class="well well-sm">
                  <div class="col-xs-12">
                    <?php
                      echo lang("gst") . ": " . $biller->gst;
                    ?>
                    <h2 class=""><?= $biller->company != '-' ? $biller->company : $biller->name; ?></h2>
                    <?= $biller->company ? "" : "Attn: " . $biller->name ?>

                    <?php
                    echo $biller->address . ", " . $biller->city . ", " . $biller->postal_code . ", " . $this->site->getStateByID($biller->state)->name . ", " . $biller->country;
                    echo " -- ";
                    echo lang("tel") . ": " . $biller->phone . " " . lang("email") . ": " . $biller->email;
                    ?>
                  </div>
                  <div class="col-xs-12">
                    <span>======================================================================================================================================</span>
                  </div>
                  <div class="col-xs-12">
                    <div class="col-xs-6">
                      <div class="col-xs-3"> <?= lang("BILL NO ") ?> </div>
                      <div class="col-xs-3"> :: <?php echo $inv->id ?> </div>
                      <br>
                      <div class="col-xs-3"> <?= lang("Date ") ?> </div>
                      <div class="col-xs-6"> :: <?= $this->sma->hrsd($inv->date); ?> </div>
                      <br>
                      <div class="col-xs-3"> State Code </div>
                      <div class="col-xs-6"> :: <?php echo $customer->state ? $customer->state : '---'; ?> </div>
                    </div>
                    <div class="col-xs-6">
                      <div class="col-xs-12">
                        <b><?= $customer->company ? $customer->company : $customer->name; ?></b>
                      </div>
                      <div class="col-xs-12">
                        <?= $customer->address . ", " . $customer->city . ", " . $customer->postal_code . ", " . $this->site->getStateByID($customer->state)->name . ", " . $customer->country; ?>
                      </div>
                      <!-- <br> -->
                      <div class="col-xs-1"> <?= lang('GST:: ') ?> </div>
                      <div class="col-xs-9">
                        <?php
                        if ($customer->gst != "-" && $customer->gst != "") {
                            echo $customer->gst;
                        }
                        ?>
                      </div>

                    </div>
                  </div>
                  <div class="col-xs-12">
                    <span>======================================================================================================================================</span>
                  </div>




                  <div class="clearfix"></div>
                  <div class="col-xs-12">
                    <div class="table">
                        <table class="table table-borderless">

                            <thead>

                            <tr>
                                <th><?= lang("SL"); ?></th>
                                <th><?= lang("ITEM DESCRIPTION"); ?></th>
                                <th><?= lang("QTY"); ?></th>
                                <th><?= lang("HSN"); ?></th>
                                <?php
                                // if ($Settings->product_serial) {
                                //     echo '<th style="text-align:center; vertical-align:middle;">' . lang("serial_no") . '</th>';
                                // }
                                ?>
                                <th style="padding-right:20px;"><?= lang("Mrp"); ?></th>
                                <th style="padding-right:20px;"><?= lang("unit_price"); ?></th>
                                <?php
                                if ($Settings->tax1 && $inv->product_tax > 0) {
                                    echo '<th style="padding-right:20px; text-align:center; vertical-align:middle;">' . lang("CGST") . '</th>';
                                    echo '<th style="padding-right:20px; text-align:center; vertical-align:middle;">' . lang("SGST") . '</th>';
                                }
                                if ($Settings->product_discount && $inv->product_discount != 0) {
                                    echo '<th style="padding-right:20px; text-align:center; vertical-align:middle;">' . lang("discount") . '</th>';
                                }
                                ?>
                                <th style="padding-right:20px;"><?= lang("subtotal"); ?></th>
                            </tr>

                            </thead>

                            <tbody>
                              <?php $col = 9; ?>
                              <tr>
                                <td colspan="<?= $col ?>">======================================================================================================================================</td>
                              </tr>
                            <?php $r = 1;
                            $tax_summary = array();
                            $tax_0 = 0;
                            $tax_5 = 0;
                            $tax_12 = 0;
                            $tax_18 = 0;
                            $tax_28 = 0;
                            $tax_0_amt = 0;
                            $tax_5_amt = 0;
                            $tax_12_amt = 0;
                            $tax_18_amt = 0;
                            $tax_28_amt = 0;
                            foreach ($rows as $row):
                                if (isset($tax_summary[$row->tax_code])) {
                                    $tax_summary[$row->tax_code]['items'] += $row->unit_quantity;
                                    $tax_summary[$row->tax_code]['tax'] += $row->item_tax;
                                    $tax_summary[$row->tax_code]['amt'] += ($row->unit_quantity * $row->net_unit_price) - $row->item_discount;
                                } else {
                                    $tax_summary[$row->tax_code]['items'] = $row->unit_quantity;
                                    $tax_summary[$row->tax_code]['tax'] = $row->item_tax;
                                    $tax_summary[$row->tax_code]['amt'] = ($row->unit_quantity * $row->net_unit_price) - $row->item_discount;
                                    $tax_summary[$row->tax_code]['name'] = $row->tax_name;
                                    $tax_summary[$row->tax_code]['code'] = $row->tax_code;
                                    $tax_summary[$row->tax_code]['rate'] = $row->tax_rate;
                                }
                                ?>
                                <tr>
                                    <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                    <td style="vertical-align:center; text-align:center">
                                        <?= $row->product_name . ($row->variant ? ' (' . $row->variant . ')' : '') . ' ('.$row->product_code.')'; ?>
                                        <?= $row->details ? '<br>' . $row->details : ''; ?> </td>
                                    <td style="width: 100px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->unit_quantity).' ('.$row->product_unit_code . ')'; ?></td>
                                    <?php $product_details = $this->site->getProductByID($row->product_id); ?>
                                    <td style="width: 100px; text-align:center; vertical-align:middle;"><?= $product_details->hsn_code; ?></td>
                                    <td style="width: 100px; text-align:center; vertical-align:middle;"><?= $this->sma->formatMoney($product_details->mrp); ?></td>
                                    <?php
                                    // if ($Settings->product_serial) {
                                    //     echo '<td>' . $row->serial_no . '</td>';
                                    // }
                                    ?>
                                    <td style="text-align:right; width:120px; padding-right:10px;"><?= $this->sma->formatMoney($row->real_unit_price); ?></td>
                                    <?php
                                    if ($Settings->tax1 && $inv->product_tax > 0) {
                                      if ($row->tax_code == 'GST 0%') {
                                        $tax_0_amt += $row->real_unit_price * $row->unit_quantity;
                                      }
                                      if ($row->tax_code == 'GST 5%') {
                                        $tax_5_amt += $row->real_unit_price * $row->unit_quantity;
                                      }
                                      if ($row->tax_code == 'GST 12%') {
                                        $tax_12_amt += $row->real_unit_price * $row->unit_quantity;
                                      }
                                      if ($row->tax_code == 'GST 18%') {
                                        $tax_18_amt += $row->real_unit_price * $row->unit_quantity;
                                      }
                                      if ($row->tax_code == 'GST 28%') {
                                        $tax_28_amt += $row->real_unit_price * $row->unit_quantity;
                                      }
                                      if ($row->tax_code == 'GST 0%') {
                                        $tax_0 += $row->item_tax;
                                      }
                                      if ($row->tax_code == 'GST 5%') {
                                        $tax_5 += $row->item_tax / 2;
                                      }
                                      if ($row->tax_code == 'GST 12%') {
                                        $tax_12 += $row->item_tax / 2;
                                      }
                                      if ($row->tax_code == 'GST 18%') {
                                        $tax_18 += $row->item_tax / 2;
                                      }
                                      if ($row->tax_code == 'GST 28%') {
                                        $tax_28 += $row->item_tax / 2;
                                      }
                                        echo '<td style="width: 120px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 ? '<small>('.($Settings->indian_gst ? $row->tax / 2 : $row->tax_code / 2).'%)</small>' : '') . ' ' . $this->sma->formatMoney($row->item_tax / 2) . '</td>';
                                        echo '<td style="width: 120px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 ? '<small>('.($Settings->indian_gst ? $row->tax / 2 : $row->tax_code / 2).'%)</small>' : '') . ' ' . $this->sma->formatMoney($row->item_tax / 2) . '</td>';
                                    }
                                    if ($Settings->product_discount && $inv->product_discount != 0) {
                                        echo '<td style="width: 120px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) . '</td>';
                                    }
                                    ?>
                                    <td style="text-align:right; width:120px; padding-right:10px;"><?= $this->sma->formatMoney($row->subtotal); ?></td>
                                </tr>
                                <?php
                                $r++;
                            endforeach;

                            if ($return_rows) {
                                echo '<tr class="warning"><td colspan="100%" class="no-border"><strong>'.lang('returned_items').'</strong></td></tr>';
                                foreach ($return_rows as $row):
                                    if (isset($tax_summary[$row->tax_code])) {
                                        $tax_summary[$row->tax_code]['items'] += $row->unit_quantity;
                                        $tax_summary[$row->tax_code]['tax'] += $row->item_tax;
                                        $tax_summary[$row->tax_code]['amt'] += ($row->unit_quantity * $row->net_unit_price) - $row->item_discount;
                                    } else {
                                        $tax_summary[$row->tax_code]['items'] = $row->unit_quantity;
                                        $tax_summary[$row->tax_code]['tax'] = $row->item_tax;
                                        $tax_summary[$row->tax_code]['amt'] = ($row->unit_quantity * $row->net_unit_price) - $row->item_discount;
                                        $tax_summary[$row->tax_code]['name'] = $row->tax_name;
                                        $tax_summary[$row->tax_code]['code'] = $row->tax_code;
                                        $tax_summary[$row->tax_code]['rate'] = $row->tax_rate;
                                    }
                                    ?>
                                    <tr class="warning">
                                        <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                        <td style="vertical-align:middle;">
                                            <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                            <?= $row->details ? '<br>' . $row->details : ''; ?> </td>
                                        <td style="width: 100px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity).' '.$row->product_unit_code; ?></td>
                                        <?php
                                        if ($Settings->product_serial) {
                                            echo '<td>' . $row->serial_no . '</td>';
                                        }
                                        ?>
                                        <td style="text-align:right; width:120px; padding-right:10px;"><?= $this->sma->formatMoney($row->real_unit_price); ?></td>
                                        <?php
                                        if ($Settings->tax1 && $inv->product_tax > 0) {
                                            echo '<td style="width: 120px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 ? '<small>('.($Settings->indian_gst ? $row->tax : $row->tax_code).')</small>' : '') . ' ' . $this->sma->formatMoney($row->item_tax) . '</td>';
                                        }
                                        if ($Settings->product_discount && $inv->product_discount != 0) {
                                            echo '<td style="width: 120px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) . '</td>';
                                        }
                                        ?>
                                        <td style="text-align:right; width:120px; padding-right:10px;"><?= $this->sma->formatMoney($row->subtotal); ?></td>
                                    </tr>
                                    <?php
                                    $r++;
                                endforeach;
                            }

                            ?>
                            </tbody>
                            <tfoot>
                            <?php
                            $col = 7;
                            if ($Settings->product_serial) {
                                $col++;
                            }
                            if ($Settings->product_discount && $inv->product_discount != 0) {
                                $col++;
                            }
                            if ($Settings->tax1 && $inv->product_tax > 0) {
                                $col++;
                            }
                            if ($Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1 && $inv->product_tax > 0) {
                                $tcol = $col - 2;
                            } elseif ($Settings->product_discount && $inv->product_discount != 0) {
                                $tcol = $col - 1;
                            } elseif ($Settings->tax1 && $inv->product_tax > 0) {
                                $tcol = $col - 1;
                            } else {
                                $tcol = $col;
                            }
                            $tcol = $tcol;
                            ?>
                            <?php
                            if ($inv->grand_total != $inv->total) { ?>
                                <!-- <tr>
                                    <td colspan="<?= $tcol + 1; ?>"
                                        style="text-align:right; padding-right:10px;"><?= lang("total"); ?>
                                        (<?= $default_currency->code; ?>)
                                    </td>
                                    <?php
                                    if ($Settings->tax1 && $inv->product_tax > 0) {
                                        // echo '<td style="text-align:right;">' . $this->sma->formatMoney($return_sale ? ($inv->product_tax+$return_sale->product_tax) : $inv->product_tax) . '</td>';
                                    }
                                    if ($Settings->product_discount && $inv->product_discount != 0) {
                                        echo '<td style="text-align:right;">' . $this->sma->formatMoney($return_sale ? ($inv->product_discount+$return_sale->product_discount) : $inv->product_discount) . '</td>';
                                    }
                                    ?>
                                    <td style="text-align:right; padding-right:10px;"><?= $this->sma->formatMoney($return_sale ? (($inv->total + $inv->product_tax)+($return_sale->total + $return_sale->product_tax)) : ($inv->total + $inv->product_tax)); ?></td>
                                </tr> -->
                            <?php } ?>
                            <?php
                            // if ($return_sale) {
                            //     echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_total") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale->grand_total) . '</td></tr>';
                            // }
                            // if ($inv->surcharge != 0) {
                            //     echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_surcharge") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->surcharge) . '</td></tr>';
                            // }
                            ?>

                            <?php
                          //     if ($Settings->indian_gst) {
                          //     if ($inv->cgst > 0) {
                          //         $cgst = $return_sale ? $inv->cgst + $return_sale->cgst : $inv->cgst;
                          //         echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('cgst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($cgst) : $cgst) . '</td></tr>';
                          //     }
                          //     if ($inv->sgst > 0) {
                          //         $sgst = $return_sale ? $inv->sgst + $return_sale->sgst : $inv->sgst;
                          //         echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('sgst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($sgst) : $sgst) . '</td></tr>';
                          //     }
                          //     if ($inv->igst > 0) {
                          //         $igst = $return_sale ? $inv->igst + $return_sale->igst : $inv->igst;
                          //         echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('gst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($igst) : $igst) . '</td></tr>';
                          //     }
                          // }
                        ?>

                            <?php
                              // if ($inv->order_discount != 0) {
                              //     echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("order_discount") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">'.($inv->order_discount_id ? '<small>('.$inv->order_discount_id.')</small> ' : '') . $this->sma->formatMoney($return_sale ? ($inv->order_discount+$return_sale->order_discount) : $inv->order_discount) . '</td></tr>';
                              // }
                              ?>
                              <?php
                              //  if ($Settings->tax2 && $inv->order_tax != 0) {
                              //     echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;">' . lang("order_tax") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale ? ($inv->order_tax+$return_sale->order_tax) : $inv->order_tax) . '</td></tr>';
                              // }
                              ?>
                              <?php
                              // if ($inv->shipping != 0) {
                              //     echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("shipping") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->shipping) . '</td></tr>';
                              // }
                            ?>
                            <!-- <tr>
                                <td colspan="<?= $col; ?>"
                                    style="text-align:right; font-weight:bold;"><?= lang("total_amount"); ?>
                                    (<?= $default_currency->code; ?>)
                                </td>
                                <td style="text-align:right; padding-right:10px; font-weight:bold;"><?= $this->sma->formatMoney($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total); ?></td>
                            </tr>
                            <tr>
                                <td colspan="<?= $col; ?>"
                                    style="text-align:right; font-weight:bold;"><?= lang("paid"); ?>
                                    (<?= $default_currency->code; ?>)
                                </td>
                                <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid); ?></td>
                            </tr>
                            <tr>
                                <td colspan="<?= $col; ?>"
                                    style="text-align:right; font-weight:bold;"><?= lang("balance"); ?>
                                    (<?= $default_currency->code; ?>)
                                </td>
                                <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney(($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total) - ($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid)); ?></td>
                            </tr> -->

                            </tfoot>
                        </table>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <span>======================================================================================================================================</span>
                  </div>
                  <div class="col-xs-12">
                    <div class="col-xs-6"> GST TOTAL &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:: <?= $this->sma->formatMoney($inv->igst); ?></div>
                    <!-- <div class="col-xs-2">  </div> -->
                    <div class="col-xs-4"> </div>
                    <div class="col-xs-2"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total :: <?= $this->sma->formatMoney($inv->grand_total); ?></div>
                    <!-- <div class="col-xs-2">  </div> -->
                  </div>
                  <?php if ($tax_0_amt != ''): ?>
                    <div class="col-xs-12">
                      <div class="col-xs-3">
                        Total 0% Sales &nbsp;&nbsp;&nbsp;:: <?= $this->sma->formatMoney($tax_0_amt); ?>
                      </div>
                      <div class="col-xs-3">
                        Total 0% CGST &nbsp;&nbsp;&nbsp;:: <?= $this->sma->formatMoney($tax_0); ?>
                      </div>
                      <div class="col-xs-3">
                        Total 0% SGST &nbsp;&nbsp;&nbsp;:: <?= $this->sma->formatMoney($tax_0); ?>
                      </div>
                    </div>
                  <?php endif; ?>
                  <?php if ($tax_5_amt != ''): ?>
                    <div class="col-xs-12">
                      <div class="col-xs-3">
                        Total 5% Sales &nbsp;&nbsp;&nbsp;:: <?= $this->sma->formatMoney($tax_5_amt); ?>
                      </div>
                      <div class="col-xs-3">
                        Total 2.5% CGST &nbsp;&nbsp;&nbsp;:: <?= $this->sma->formatMoney($tax_5); ?>
                      </div>
                      <div class="col-xs-3">
                        Total 2.5% SGST &nbsp;&nbsp;&nbsp;:: <?= $this->sma->formatMoney($tax_5); ?>
                      </div>
                    </div>
                  <?php endif; ?>
                  <?php if ($tax_12_amt != ''): ?>
                    <div class="col-xs-12">
                      <div class="col-xs-3">
                        Total 12% Sales &nbsp;&nbsp;&nbsp;:: <?= $this->sma->formatMoney($tax_12_amt); ?>
                      </div>
                      <div class="col-xs-3">
                        Total 6% CGST &nbsp;&nbsp;&nbsp;:: <?= $this->sma->formatMoney($tax_12); ?>
                      </div>
                      <div class="col-xs-3">
                        Total 6% SGST &nbsp;&nbsp;&nbsp;:: <?= $this->sma->formatMoney($tax_12); ?>
                      </div>
                    </div>
                  <?php endif; ?>
                  <?php if ($tax_18_amt != ''): ?>
                    <div class="col-xs-12">
                      <div class="col-xs-3">
                        Total 18% Sales &nbsp;&nbsp;&nbsp;:: <?= $this->sma->formatMoney($tax_18_amt); ?>
                      </div>
                      <div class="col-xs-3">
                        Total 9% CGST &nbsp;&nbsp;&nbsp;:: <?= $this->sma->formatMoney($tax_18); ?>
                      </div>
                      <div class="col-xs-3">
                        Total 9% SGST &nbsp;&nbsp;&nbsp;:: <?= $this->sma->formatMoney($tax_18); ?>
                      </div>
                    </div>
                  <?php endif; ?>
                  <?php if ($tax_28_amt != ''): ?>
                    <div class="col-xs-12">
                      <div class="col-xs-3">
                        Total 28% Sales &nbsp;&nbsp;&nbsp;:: <?= $this->sma->formatMoney($tax_28_amt); ?>
                      </div>
                      <div class="col-xs-3">
                        Total 14% CGST &nbsp;&nbsp;&nbsp;:: <?= $this->sma->formatMoney($tax_28); ?>
                      </div>
                      <div class="col-xs-3">
                        Total 14% SGST &nbsp;&nbsp;&nbsp;:: <?= $this->sma->formatMoney($tax_28); ?>
                      </div>
                    </div>
                  <?php endif; ?>

                  <div class="col-xs-12">
                    <span>======================================================================================================================================</span>
                  </div>
                  <!-- <div class="col-xs-5"> </div> -->
                  <div class="col-xs-12 text-center">
                    <p>GST BILLING AND ACCOUNTING SOFTWARE,
                      For any business only Rs.10000, one time payment for lifetime use free maintenance.  </p>
                    <p>NO hidden charges Contact: 0877-6561199, 9700111444, 9640195798</p>
                  </div>









                </div>




                <div class="clearfix"></div>
                <div class="col-md-12">
                  <br>
                </div>


                <?php if ($inv->payment_status != 'paid') { ?>
                    <div id="payment_buttons" class="row text-center padding10 no-print">

                        <?php if ($paypal->active == "1" && $inv->grand_total != "0.00") {
                            if (trim(strtolower($customer->country)) == $biller->country) {
                                $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_my / 100);
                            } else {
                                $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_other / 100);
                            }
                            ?>
                            <div class="col-xs-6 text-center">
                                <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                    <input type="hidden" name="cmd" value="_xclick">
                                    <input type="hidden" name="business" value="<?= $paypal->account_email; ?>">
                                    <input type="hidden" name="item_name" value="<?= $inv->reference_no; ?>">
                                    <input type="hidden" name="item_number" value="<?= $inv->id; ?>">
                                    <input type="hidden" name="image_url"
                                           value="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>">
                                    <input type="hidden" name="amount"
                                           value="<?= ($inv->grand_total - $inv->paid) + $paypal_fee; ?>">
                                    <input type="hidden" name="no_shipping" value="1">
                                    <input type="hidden" name="no_note" value="1">
                                    <input type="hidden" name="currency_code" value="<?= $default_currency->code; ?>">
                                    <input type="hidden" name="bn" value="FC-BuyNow">
                                    <input type="hidden" name="rm" value="2">
                                    <input type="hidden" name="return"
                                           value="<?= admin_url('sales/view/' . $inv->id); ?>">
                                    <input type="hidden" name="cancel_return"
                                           value="<?= admin_url('sales/view/' . $inv->id); ?>">
                                    <input type="hidden" name="notify_url"
                                           value="<?= admin_url('payments/paypalipn'); ?>"/>
                                    <input type="hidden" name="custom"
                                           value="<?= $inv->reference_no . '__' . ($inv->grand_total - $inv->paid) . '__' . $paypal_fee; ?>">
                                    <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block"><i
                                            class="fa fa-money"></i> <?= lang('pay_by_paypal') ?></button>
                                </form>
                            </div>
                        <?php } ?>


                        <div class="clearfix"></div>
                    </div>
                <?php } ?>

            </div>
        </div>
        <?php if (!$Supplier || !$Customer) { ?>
            <div class="buttons">
                <div class="btn-group btn-group-justified">
                    <?php if ($inv->attachment) { ?>
                        <div class="btn-group">
                            <a href="<?= admin_url('welcome/download/' . $inv->attachment) ?>" class="tip btn btn-primary" title="<?= lang('attachment') ?>">
                                <i class="fa fa-chain"></i>
                                <span class="hidden-sm hidden-xs"><?= lang('attachment') ?></span>
                            </a>
                        </div>
                    <?php } ?>
                    <div class="btn-group">
                        <a href="<?= admin_url('sales/payments/' . $inv->id) ?>" data-toggle="modal" data-target="#myModal" class="tip btn btn-primary tip" title="<?= lang('view_payments') ?>">
                            <i class="fa fa-money"></i> <span class="hidden-sm hidden-xs"><?= lang('view_payments') ?></span>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a href="<?= admin_url('sales/add_payment/' . $inv->id) ?>" data-toggle="modal" data-target="#myModal" class="tip btn btn-primary tip" title="<?= lang('add_payment') ?>">
                            <i class="fa fa-money"></i> <span class="hidden-sm hidden-xs"><?= lang('add_payment') ?></span>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a href="<?= admin_url('sales/email/' . $inv->id) ?>" data-toggle="modal" data-target="#myModal" class="tip btn btn-primary tip" title="<?= lang('email') ?>">
                            <i class="fa fa-envelope-o"></i> <span class="hidden-sm hidden-xs"><?= lang('email') ?></span>
                        </a>
                    </div>
                    <div class="btn-group">
                        <span class="tip btn btn-primary" title="<?= lang('print') ?>" onclick="myPrint()">
                            <i class="fa fa-print"></i> <span class="hidden-sm hidden-xs"><?= lang('print') ?></span>
                        </span>
                    </div>
                    <script>
                      function myPrint() {
                          window.print();
                      }
                    </script>
                    <?php if ( ! $inv->sale_id) { ?>
                    <div class="btn-group">
                        <a href="<?= admin_url('sales/add_delivery/' . $inv->id) ?>" data-toggle="modal" data-target="#myModal" class="tip btn btn-primary tip" title="<?= lang('add_delivery') ?>">
                            <i class="fa fa-truck"></i> <span class="hidden-sm hidden-xs"><?= lang('add_delivery') ?></span>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a href="<?= admin_url('sales/edit/' . $inv->id) ?>" class="tip btn btn-warning tip sledit" title="<?= lang('edit') ?>">
                            <i class="fa fa-edit"></i> <span class="hidden-sm hidden-xs"><?= lang('edit') ?></span>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a href="#" class="tip btn btn-danger bpo"
                            title="<b><?= $this->lang->line("delete_sale") ?></b>"
                            data-content="<div style='width:150px;'><p><?= lang('r_u_sure') ?></p><a class='btn btn-danger' href='<?= admin_url('sales/delete/' . $inv->id) ?>'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button></div>"
                            data-html="true" data-placement="top"><i class="fa fa-trash-o"></i>
                            <span class="hidden-sm hidden-xs"><?= lang('delete') ?></span>
                        </a>
                    </div>
                    <?php } ?>
                    <!--<div class="btn-group"><a href="<?= admin_url('sales/excel/' . $inv->id) ?>" class="tip btn btn-primary"  title="<?= lang('download_excel') ?>"><i class="fa fa-download"></i> <?= lang('excel') ?></a></div>-->
                </div>
            </div>
        <?php } ?>
    </div>
</div>
