<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-upload"></i><?= lang('updates'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('update_heading'); ?></p>

                <div class="row">
                    <div class="col-md-12">
                      <form class="" action="" method="post">
                        <!-- <?= form_submit('check_update', lang('Check update'), 'class="btn btn-primary"'); ?> -->
                      </form>
                        <?php
                        if(!$Settings->purchase_code || !$Settings->envato_username) {
                            echo admin_form_open('system_settings/updates');
                        ?>
                            <div class="form-group">
                                <?= lang('purchase_code', 'purchase_code'); ?>
                                <?= form_input('purchase_code', '', 'class="form-control tip" id="purchase_code"  required="required"'); ?>
                            </div>
                            <div class="form-group">
                                <?= lang('envato_username', 'envato_username'); ?>
                                <?= form_input('envato_username', '', 'class="form-control tip" id="envato_username"  required="required"'); ?>
                            </div>
                            <div class="form-group">
                                <?= form_submit('update', lang('update'), 'class="btn btn-primary"'); ?>
                            </div>
                        <?php
                            echo form_close();
                        } else {
                            // print_r($updates->data);
                            if (!empty($updates->data->updates)) {
                                $c = 1;
                                foreach ($updates as $update) {
                                  echo "<br>";
                                  print_r($update->updates->filename);
                                    echo '<ul class="list-group"><li class="list-group-item">';
                                    echo '<h3><strong>' . lang('version') . ' ' . $update->updates->version . '</strong> ';
                                    if ($c == 1 && !empty($update->updates->filename)) {
                                        echo anchor('admin/system_settings/install_update/' . substr($update->updates->filename, 0, -4) . '/' . (!empty($update->updates->m_version) ? $update->updates->m_version : 0) . '/' . $update->updates->version, '<i class="fa fa-download"></i> ' . lang('install'), 'class="btn btn-primary pull-right"') . ' ';
                                    }
                                    echo '</h3><h3>' . lang('changelog') . '<h3><pre>' . $update->updates->changelog . '</pre>';
                                    echo '</li></ul>';
                                    $c++;
                                }
                            } else {

                                echo '<div class="well"><strong>' . lang('using_latest_update') . '</strong></div>';
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
